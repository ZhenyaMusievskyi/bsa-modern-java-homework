package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirements;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.name = name;
		attackSubsystem.powergridRequirements = powergridRequirements;
		attackSubsystem.capacitorConsumption = capacitorConsumption;
		attackSubsystem.optimalSpeed = optimalSpeed;
		attackSubsystem.optimalSize = optimalSize;
		attackSubsystem.baseDamage = baseDamage;
		return attackSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().greaterOrEqual(this.optimalSize) ? 1
				: (double) target.getSize().value() / this.optimalSize.value();

		double speedReductionModifier = target.getCurrentSpeed().lowerOrEqual(this.optimalSpeed) ? 1
				: (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());

		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));

		return PositiveInteger.of(damage);
	}

}
