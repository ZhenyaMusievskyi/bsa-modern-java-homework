package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.name = name;
		defenciveSubsystem.powergridConsumption = powergridConsumption;
		defenciveSubsystem.capacitorConsumption = capacitorConsumption;
		defenciveSubsystem.impactReductionPercent = impactReductionPercent;
		defenciveSubsystem.shieldRegeneration = shieldRegeneration;
		defenciveSubsystem.hullRegeneration = hullRegeneration;

		return defenciveSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int impactReductionPercent = this.impactReductionPercent.value() < 95 ? this.impactReductionPercent.value()
				: 95;
		int damage = incomingDamage.damage.value();
		int resultDamage = (int) Math.ceil(damage * (1 - impactReductionPercent / 100.0));

		return new AttackAction(PositiveInteger.of(resultDamage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
