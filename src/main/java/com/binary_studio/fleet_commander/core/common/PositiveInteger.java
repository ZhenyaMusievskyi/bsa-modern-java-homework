package com.binary_studio.fleet_commander.core.common;

public final class PositiveInteger {

	private final Integer underlyingVal;

	public PositiveInteger(Integer val) {
		if (val < 0) {
			throw new IllegalArgumentException(String.format("Got negative value %d, expected positive integer", val));
		}
		this.underlyingVal = val;
	}

	public static PositiveInteger of(Integer val) {
		return new PositiveInteger(val);
	}

	public Integer value() {
		return this.underlyingVal;
	}

	public boolean greater(PositiveInteger x) {
		return this.underlyingVal > x.value();
	}

	public boolean lower(PositiveInteger x) {
		return this.underlyingVal < x.value();
	}

	public boolean greaterOrEqual(PositiveInteger x) {
		return this.underlyingVal >= x.value();
	}

	public boolean lowerOrEqual(PositiveInteger x) {
		return this.underlyingVal <= x.value();
	}

	public PositiveInteger addAndGet(PositiveInteger x) {
		return new PositiveInteger(this.underlyingVal + x.value());
	}

	public PositiveInteger subtractAndGet(PositiveInteger x) {
		return new PositiveInteger(this.underlyingVal - x.value());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != PositiveInteger.class) {
			return false;
		}
		return ((PositiveInteger) obj).underlyingVal == this.underlyingVal;
	}

	@Override
	public int hashCode() {
		return this.underlyingVal.hashCode();
	}

}
