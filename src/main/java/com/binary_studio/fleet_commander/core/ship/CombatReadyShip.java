package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private final PositiveInteger initialShieldHP;

	private PositiveInteger hullHP;

	private final PositiveInteger initialHullHP;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger initialCapacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger powergridOutput;

	private PositiveInteger size;

	private PositiveInteger currentSpeed;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger powergridOutput,
			PositiveInteger size, PositiveInteger currentSpeed, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.initialShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.initialHullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.initialCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.powergridOutput = powergridOutput;
		this.size = size;
		this.currentSpeed = currentSpeed;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void startTurn() {
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = this.capacitorAmount.addAndGet(this.capacitorRechargeRate);
		if (this.capacitorAmount.greater(this.initialCapacitorAmount)) {
			this.capacitorAmount = this.initialCapacitorAmount;
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.currentSpeed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attackSubsystem.getCapacitorConsumption().greater(this.capacitorAmount)) {
			return Optional.empty();
		}

		PositiveInteger damage = this.attackSubsystem.attack(target);
		AttackAction attackAction = new AttackAction(damage, this, target, this.attackSubsystem);
		this.capacitorAmount = this.capacitorAmount.subtractAndGet(this.attackSubsystem.getCapacitorConsumption());
		return Optional.of(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		if (this.capacitorAmount.greaterOrEqual(this.defenciveSubsystem.getCapacitorConsumption())) {
			attack = this.defenciveSubsystem.reduceDamage(attack);
			this.capacitorAmount = this.capacitorAmount
					.subtractAndGet(this.defenciveSubsystem.getCapacitorConsumption());
		}

		AttackResult result = new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);

		int damage = attack.damage.value();
		int shieldHP = this.shieldHP.value();
		if (shieldHP > 0) {
			if (shieldHP >= damage) {
				shieldHP -= damage;
				damage = 0;
			}
			else {
				damage -= shieldHP;
				shieldHP = 0;
			}

			this.shieldHP = PositiveInteger.of(shieldHP);
		}

		int hullHP = this.hullHP.value();
		if (hullHP > 0 && damage > 0) {
			if (hullHP > damage) {
				hullHP -= damage;
			}
			else {
				hullHP = 0;
			}

			this.hullHP = PositiveInteger.of(hullHP);
		}

		if (this.shieldHP.value() == 0 && this.hullHP.value() == 0) {
			result = new AttackResult.Destroyed();
		}

		return result;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.lower(this.defenciveSubsystem.getCapacitorConsumption())) {
			return Optional.empty();
		}

		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		int shieldHPRegenerated = regenerateAction.shieldHPRegenerated.value();
		int hullHPRegenerated = regenerateAction.hullHPRegenerated.value();

		this.shieldHP = PositiveInteger.of(this.shieldHP.value() + shieldHPRegenerated);
		this.hullHP = PositiveInteger.of(this.hullHP.value() + hullHPRegenerated);

		if (this.shieldHP.greater(this.initialShieldHP)) {
			shieldHPRegenerated -= this.shieldHP.value() - this.initialShieldHP.value();
			this.shieldHP = this.initialShieldHP;
		}

		if (this.hullHP.greater(this.initialHullHP)) {
			hullHPRegenerated -= this.hullHP.value() - this.initialHullHP.value();
			this.hullHP = this.initialHullHP;
		}

		regenerateAction = new RegenerateAction(PositiveInteger.of(shieldHPRegenerated),
				PositiveInteger.of(hullHPRegenerated));
		if (regenerateAction.hullHPRegenerated.value() != 0 || regenerateAction.shieldHPRegenerated.value() != 0) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		}

		return Optional.of(regenerateAction);
	}

}
