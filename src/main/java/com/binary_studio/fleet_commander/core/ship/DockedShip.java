package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger powergridOutput;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DockedShip ship = new DockedShip();
		ship.name = name;
		ship.shieldHP = shieldHP;
		ship.hullHP = hullHP;
		ship.powergridOutput = powergridOutput;
		ship.capacitorAmount = capacitorAmount;
		ship.capacitorRechargeRate = capacitorRechargeRate;
		ship.speed = speed;
		ship.size = size;

		return ship;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}

		int powerGridUsing = this.defenciveSubsystem != null ? this.defenciveSubsystem.getPowerGridConsumption().value()
				: 0;
		powerGridUsing += subsystem.getPowerGridConsumption().value();

		if (powerGridUsing > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(powerGridUsing - this.powergridOutput.value());
		}

		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}

		int powerGridUsing = this.attackSubsystem != null ? this.attackSubsystem.getPowerGridConsumption().value() : 0;
		powerGridUsing += subsystem.getPowerGridConsumption().value();

		if (powerGridUsing > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(powerGridUsing - this.powergridOutput.value());
		}

		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		NotAllSubsystemsFitted exception = this.attackSubsystem == null
				? (this.defenciveSubsystem == null ? NotAllSubsystemsFitted.bothMissing()
						: NotAllSubsystemsFitted.attackMissing())
				: (this.defenciveSubsystem == null ? NotAllSubsystemsFitted.defenciveMissing() : null);

		if (exception != null) {
			throw exception;
		}

		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.powergridOutput, this.size, this.speed, this.attackSubsystem,
				this.defenciveSubsystem);
	}

}
