package com.binary_studio.tree_max_depth;

import java.util.Stack;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		int maxLength = 1;
		Stack<Node> s = new Stack<>();
		Node curr = new Node(rootDepartment);

		// traverse the tree
		while (curr.department.subDepartments.size() > 0) {
			// reach the last Node of the curr Node using currentChildIndex to choose
			// branch
			while (curr.department != null && curr.department.subDepartments.size() > 0
					&& curr.department.subDepartments.size() > curr.currentChildIndex) {

				// place reference to a tree node on the stack before traversing the
				// node's subtree
				s.push(curr);
				curr = new Node(curr.department.subDepartments.get(curr.currentChildIndex++));
			}

			if (s.size() + 1 > maxLength && curr.department != null) {
				maxLength = s.size() + 1;
			}

			if (s.size() == 0) {
				break;
			}
			curr = s.pop();
		}

		return maxLength;
	}

	// wrapper for Department to save child index that we will use next
	private static class Node {

		private Department department;

		private int currentChildIndex;

		Node(Department department) {
			this.department = department;
		}

	}

}
