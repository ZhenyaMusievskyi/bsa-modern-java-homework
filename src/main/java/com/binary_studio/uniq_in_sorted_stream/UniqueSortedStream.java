package com.binary_studio.uniq_in_sorted_stream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(new Predicate<Row<T>>() {
			private long prevId = Long.MIN_VALUE;

			@Override
			public boolean test(Row<T> row) {
				if (row.getPrimaryId() > this.prevId) {
					this.prevId = row.getPrimaryId();
					return true;
				}

				return false;
			}
		});
	}

}
